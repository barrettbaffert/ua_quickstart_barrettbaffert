7.x-1.0-alpha58, 2020-06-17
-----------------------------

7.x-1.0-alpha57, 2020-06-05
-----------------------------

7.x-1.0-alpha56, 2020-05-20
-----------------------------

7.x-1.0-alpha55, 2020-04-20
-----------------------------

7.x-1.0-alpha54, 2020-03-20
-----------------------------

7.x-1.0-alpha53, 2020-03-06
-----------------------------

7.x-1.0-alpha52, 2020-02-07
-----------------------------

7.x-1.0-alpha51, 2019-12-18
-----------------------------

7.x-1.0-alpha50, 2019-12-13
-----------------------------

7.x-1.0-alpha49, 2019-11-20
-----------------------------

7.x-1.0-alpha48, 2019-11-05
-----------------------------

7.x-1.0-alpha47, 2019-11-01
-----------------------------
- Preparing to tag 7.x-1.0-alpha46 for uaqs_admin.

7.x-1.0-alpha46, 2019-11-01
-----------------------------
- Preparing to tag 7.x-1.0-alpha45 for uaqs_admin.

7.x-1.0-alpha45, 2019-11-01
-----------------------------

7.x-1.0-alpha44, 2019-09-06
-----------------------------
- UADIGITAL-1767 Added chosen module.

7.x-1.0-alpha43, 2019-08-16
-----------------------------

7.x-1.0-alpha42, 2019-07-26
-----------------------------
- UADIGITAL-2053 Anonymous reviewer workflow should default to draft when enabled.

7.x-1.0-alpha41, 2019-06-28
-----------------------------

7.x-1.0-alpha40, 2019-05-22
-----------------------------

7.x-1.0-alpha39, 2019-05-08
-----------------------------

7.x-1.0-alpha38, 2019-05-07
-----------------------------

7.x-1.0-alpha37, 2019-04-17
-----------------------------

7.x-1.0-alpha36, 2019-03-20
-----------------------------

7.x-1.0-alpha35, 2019-03-15
-----------------------------

7.x-1.0-alpha34, 2019-03-13
-----------------------------

7.x-1.0-alpha33, 2019-02-20
-----------------------------
- UADIGITAL-1914: Bump the link contrib module version because of a security issue.

7.x-1.0-alpha32, 2019-02-11
-----------------------------
- UADIGITAL-1879: Fixed uaqs_admin PHP Warning: array_key_exists().

7.x-1.0-alpha30, 2019-02-01
-----------------------------

7.x-1.0-alpha29, 2019-01-28
-----------------------------

7.x-1.0-alpha28, 2019-01-17
-----------------------------

7.x-1.0-alpha27, 2019-01-16
-----------------------------

7.x-1.0-alpha26, 2019-01-11
-----------------------------

7.x-1.0-alpha25, 2018-11-15
-----------------------------

7.x-1.0-alpha24, 2018-11-02
-----------------------------

7.x-1.0-alpha23, 2018-10-17
-----------------------------
- UADIGITAL-1758 Add Masquerade module

7.x-1.0-alpha22, 2018-10-11
-----------------------------

7.x-1.0-alpha21, 2018-10-10
-----------------------------

7.x-1.0-alpha20, 2018-09-21
-----------------------------

7.x-1.0-alpha19, 2018-08-29
-----------------------------
- Preparing to tag 7.x-1.0-alpha17 for uaqs_admin.
- Preparing to tag 7.x-1.0-alpha16 for uaqs_admin.
- Preparing to tag 7.x-1.0-alpha15 for uaqs_admin.
- UADIGITAL-1652 Adding nofollow header to unpublished revisions.
- Adding link to external documentation for anonymous review workflow.
- UADIGITAL-1506 anonymous review staged content workflow
- Preparing to tag 7.x-1.0-alpha14 for uaqs_admin.
- Preparing to tag 7.x-1.0-alpha13 for uaqs_admin.
- Preparing to tag 7.x-1.0-alpha12 for uaqs_admin.
- Preparing to tag 7.x-1.0-alpha11 for uaqs_admin.
- Preparing to tag 7.x-1.0-alpha10 for uaqs_admin.
- UADIGITAL-1296 Default configuration change invoving user accounts and their creation
- Preparing to tag 7.x-1.0-alpha9 for uaqs_admin.
- Preparing to tag 7.x-1.0-alpha8 for uaqs_admin.
- Preparing to tag 7.x-1.0-alpha7 for uaqs_admin.
- Provide a changelog file to keep the tagging script happy.
- Copy the subtree from the ua_quickstart UADIGITAL-1111 branch.
7.x-1.0-alpha17, 2018-07-30
-----------------------------

7.x-1.0-alpha16, 2018-07-20
-----------------------------

7.x-1.0-alpha15, 2018-06-19
-----------------------------
- UADIGITAL-1652 Adding nofollow header to unpublished revisions.
- Adding link to external documentation for anonymous review workflow.
- UADIGITAL-1506 anonymous review staged content workflow

7.x-1.0-alpha14, 2018-05-18
-----------------------------

7.x-1.0-alpha13, 2018-04-25
-----------------------------

7.x-1.0-alpha12, 2018-04-20
-----------------------------

7.x-1.0-alpha11, 2018-03-30
-----------------------------

7.x-1.0-alpha10, 2018-03-09
-----------------------------
- UADIGITAL-1296 Default configuration change invoving user accounts and their creation

7.x-1.0-alpha9, 2017-09-15
-----------------------------

7.x-1.0-alpha8, 2017-09-15
-----------------------------

7.x-1.0-alpha7, 2017-08-17
-----------------------------
- Provide a changelog file to keep the tagging script happy.
- Copy the subtree from the ua_quickstart UADIGITAL-1111 branch.
